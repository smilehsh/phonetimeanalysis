package com.hmx.phonetimeanalysis;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
//delay report function  for multi thread or resume network case .
//single model
public class CSqlInstance {
    private static String TAG = "CSqlInstance";
    private static CSqlInstance  sinstance = null;
    private Context mctx;
    private HmxApplication happ;
    private SQLiteDatabase mwdb;
    public final static byte[] slock = new byte[0];
    public final String TB_PRE = "tb";
    
    public CSqlInstance (Context ctx)
    {
        mctx = ctx;
        happ= (HmxApplication) mctx.getApplicationContext();
        mwdb= happ.getWritableDatabase();   
    }
    public void setSqlDb(SQLiteDatabase db)
    {
        mwdb = db;
    }
    public SQLiteDatabase getSqlDatabase()
    {
        return mwdb;
    }
    
    public void insertCurMonth()
    {
        insertMonth(Tools.getCurMonth());
    }
    
    public boolean insertMonth(String month)
    {
       boolean ret = false;
       try
       {
           ContentValues values = new ContentValues();
           values.put("month", month);
           mwdb.insert("tbMonth", null, values);           
           mwdb.execSQL("create table " + month +"(id integer primary key autoincrement, pkgname varchar(1024) , appname varchar(256),cdate varchar(256),len long,bak int)");            
           ret = true;
           
       }catch(Exception e)
       {
           ret = false;
           Tools.Logd(TAG, e.getMessage());
       }
       return ret;
    }
    public Map<String,String> getAppInfo(String day,String tbmonth)
    {
        if(tbmonth.contains(TB_PRE) ==false)
        {
            tbmonth = TB_PRE + tbmonth;
        }
        Map<String,String> info = new HashMap<String,String>();
        synchronized(slock) 
        {
           Cursor cursor = null;
           try
           {
               cursor  =  mwdb.query(tbmonth, new String[]{"pkgname","cdate","appname","len"}, " cdate like ?", new String[]{day}, "", "", "");
               int count  = cursor.getCount();
               if(cursor != null &&  count> 0)
               {   
                   while(cursor.moveToNext()){
                           String key  = cursor.getString(cursor.getColumnIndex("pkgname"));
                           String value = cursor.getLong(cursor.getColumnIndex("len"))+"";
                           info.put(key, value);
                       }
               }

               }catch(Exception e)
               {
                   Tools.Logd(TAG, e.getMessage());
               }
               finally
               {
                   if(cursor != null)
                   {
                       cursor.close();
                       cursor = null;
                   }
               }
        }                        
        return info;
    }
    
    public String getCurMonthTb()
    {
        String tbName="";
        synchronized(slock) 
        {
           Cursor cursor = null;
           try
           {              
               cursor  =  mwdb.query("tbMonth", new String[]{"month"}, " month like ?", new String[]{TB_PRE+Tools.getCurMonth()}, "", "", "");
               int count  = cursor.getCount();
               if(cursor != null &&  count> 0)
               {   
                   while(cursor.moveToNext()){
                            tbName = cursor.getString(cursor.getColumnIndex("month"));
                            break;
                       }
               }
               if(tbName.equals(""))
               {    
                   String stbname = TB_PRE + Tools.getCurMonth();
                   if(insertMonth(stbname))
                   {
                       tbName = stbname;
                   }
               }
               }catch(Exception e)
               {
                  Tools.Logd(TAG, e.getMessage());
               }
               finally
               {
                   if(cursor != null)
                   {
                       cursor.close();
                       cursor = null;
                   }
               }
        }        
        return tbName;
    }    
    public synchronized static CSqlInstance getInstance(Context ctx)
    {
        if(sinstance == null)
        {
            sinstance = new CSqlInstance(ctx);
        }
        return sinstance;
    }
    //carmera file
    public void saveFilepathToDb(String filepath)
    {
        if(isInCarmeraTbByPath(filepath) ==false)
        {
            insertCameraFilepathToDb(filepath);
        }
    }
    
    //check file is in db
    public boolean isInCarmeraTbByPath(String path)
    {
        boolean rtn = false;
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
            try
            {
                cursor = mwdb.rawQuery("select * from camera_file_list where filepath like ?", new String[]{path});
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn = true;
                        break;
                    }
                }
            }catch(Exception e){
                
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
            }
        }
        return  rtn;
    }    
    public void insertCameraFilepathToDb(String filepath)
    {
        synchronized(Tools.lock) 
        {
            try
            {
                ContentValues values = new ContentValues();
                values.put("filepath", filepath);
                mwdb.insert("camera_file_list", null, values);
            }
            catch(Exception e)
            {
                Tools.Logd(TAG, e.getMessage());
            }
        }
    } 
    public void deleteCameraFilepathFromDb(String filepath)
    {
        synchronized(Tools.lock) 
        {
            try
            {
                String[] args = {filepath};
                mwdb.delete("camera_file_list", "filepath like ?", args);
            }
            catch(Exception e)
            {
                
            }
        }
    }
    public String getCarmeraPathFromTb()
    {
        String  rtn = "";
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
            try
            {      
               cursor = mwdb.rawQuery("select * from camera_file_list ", null);
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn =  cursor.getString(cursor.getColumnIndex("filepath"));
                        break;
                    }
                }
            }catch(Exception e){
                
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
            }
        }
        if(rtn != null && rtn.equals("") ==false)
        {
            deleteCameraFilepathFromDb(rtn);
        }
        return  rtn;
    }        
/*    
    //palyer report   
    public void saveCPlayerReportToDb(CPlayerReport report)
    {
        if(isInPlayerTbByCPlayerReport(report) == false)
        {
            insertCPlayerReportToDb(report);
        }
    }    
    
    public void insertCPlayerReportToDb(CPlayerReport report)
    {
        synchronized(Tools.lock) 
        {
          try
          {
            ContentValues values = new ContentValues();
            values.put("tpl_id", report.tpl_id);
            values.put("start", report.start);
            values.put("end", report.end);
            values.put("imei", report.imei);
            values.put("cameralevel", report.camera);
            values.put("playid", report.play_id);
            values.put("download", report.download);
            values.put("tpl_type", report.tpl_type);
            mwdb.insert("player_report_list", null, values);
          }
          catch(Exception e)
          {
              
          }
        }
    } 
    public void deleteCPlayerReportFromDb(CPlayerReport report)
    {
        synchronized(Tools.lock) 
        {
            try
            {
                String[] args = {report.tpl_id+"",report.start + ""};
                mwdb.delete("player_report_list", "tpl_id = ? and start = ?", args);
            }
            catch(Exception e)
            {
                
            }
        }
    }  
    //check file is in db
    public boolean isInPlayerTbByCPlayerReport(CPlayerReport report)
    {
        boolean rtn = false;
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
            try
            {
               cursor = mwdb.rawQuery("select * from player_report_list where tpl_id = ? and start = ? and end = ?", new String[]{report.tpl_id+"",report.start + "",report.end + ""});
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn = true;
                        break;
                    }
                }
            }catch(Exception e){
                
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
            }
        }
        return  rtn;
    }        
    public CPlayerReport getCPlayerReportFromTb()
    {
        CPlayerReport rtn = new CPlayerReport("",0,0,0,0,0,0,0);
        
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
             try
            {
                cursor = mwdb.rawQuery("select * from player_report_list ", null);
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn.imei =  cursor.getString(cursor.getColumnIndex("imei"));
                        rtn.tpl_id =  cursor.getLong(cursor.getColumnIndex("tpl_id"));
                        rtn.start =  cursor.getLong(cursor.getColumnIndex("start"));
                        rtn.end =  cursor.getLong(cursor.getColumnIndex("end"));
                        rtn.camera = cursor.getInt(cursor.getColumnIndex("cameralevel"));
                        rtn.play_id = cursor.getLong(cursor.getColumnIndex("playid"));
                        rtn.download = cursor.getLong(cursor.getColumnIndex("download"));
                        rtn.tpl_type = cursor.getLong(cursor.getColumnIndex("tpl_type"));
                        break;
                    }
                }
            }catch(Exception e){
                rtn = null;
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
            }
        }     
        return rtn;
    }    
    
//save first imei to db
    public String getImeiFromTb()
    {
        String  rtn = "";
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
            try
            {      
                cursor = mwdb.rawQuery("select * from imei_list ", null);
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn =  cursor.getString(cursor.getColumnIndex("imei"));
                        break;
                    }
                }
            }catch(Exception e){
                
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
            }
        }

        return  rtn;
    }    
    public void insertIMEIToDb(String imei)
    {
        synchronized(Tools.lock) 
        {
          try
          {
                ContentValues values = new ContentValues();
                values.put("imei",imei);
                mwdb.insert("imei_list", null, values);
          }
          catch(Exception e)
          {
             Log.e("hsh", e.getMessage());
          }
        }
    }
    public void deleteIMEIFromDB(String imei)
    {
        synchronized(Tools.lock) 
        {
            try
            {
                String[] args = {imei};

                mwdb.delete("imei_list", "imei like ?", args);
            }
            catch(Exception e)
            {
                
            }
        }
    }

    public void resetMediaFileDataFromDB() {
        // TODO Auto-generated method stub
        //first drop table  ,second create table
        mwdb.execSQL("DROP TABLE IF EXISTS " + "playlist");
        mwdb.execSQL("create table playlist(id integer primary key autoincrement, url varchar(1024) , file_name varchar(256),start long,end long,md5 varchar(64),state int,downloadtime long)");
//        mwdb.execSQL("ALTER TABLE " + "playlist" + " ADD COLUMN " + "downloadtime" + " "
//                + "long NOT NULL DEFAULT 0");
    }
    
    //save first imei to db
    public String getSeverUrlFromTb()
    {
        String  rtn = "";
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
            try
            {      
                cursor = mwdb.rawQuery("select * from tb_server_url where key like ? ", new String[]{"url"});
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn =  cursor.getString(cursor.getColumnIndex("url"));
                        break;
                    }
                }
            }catch(Exception e){
                
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
            }
        }

        return  rtn;
    }    
    public void insertSeverUrlToDb(String url)
    {
        synchronized(Tools.lock) 
        {
          try
          {
                ContentValues values = new ContentValues();
                values.put("url",url);
                values.put("key", "url");
                mwdb.insert("tb_server_url", null, values);
          }
          catch(Exception e)
          {
             Log.e("hsh", e.getMessage());
          }
        }
    }
    public void deleteSeverUrlFromDB()
    {
        synchronized(Tools.lock) 
        {
            try
            {
                String[] args = {"url"};

                mwdb.delete("tb_server_url", "key like ?", args);                
            }
            catch(Exception e)
            {
                
            }
        }
    } 
    public final static String LOCAL_MODE = "LOCAL_MODE";
    public final static String LOCAL_NATION = "LOCAL_NATION";
    public String getValueFromConfigTb(String keyname)
    {
        String  rtn = "";
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
            try
            {      
                cursor = mwdb.rawQuery("select * from tb_server_url where key like ? ", new String[]{keyname});
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn =  cursor.getString(cursor.getColumnIndex("url"));
                        break;
                    }
                }
            }catch(Exception e){
                
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
            }
        }

        return  rtn;
    }  
    public void insertValueToConfigDb(String keyname, String url)
    {
        synchronized(Tools.lock) 
        {
          try
          {
                ContentValues values = new ContentValues();
                values.put("url",url);
                values.put("key", keyname);
                mwdb.insert("tb_server_url", null, values);
          }
          catch(Exception e)
          {
             Log.e("hsh", e.getMessage());
          }
        }
    } 
    public void deleteKeyFromConfigDB(String keyname)
    {
        synchronized(Tools.lock) 
        {
            try
            {
                String[] args = {keyname};

                mwdb.delete("tb_server_url", "key like ?", args);                
            }
            catch(Exception e)
            {
                Log.e("hsh", e.getMessage());
            }
        }
    }     
    public void deleteDataInDb(String tbname,String limit ,String[] args)
    {
        synchronized(Tools.lock) 
        {
            try
            {
                mwdb.delete(tbname, limit, args);                
            }
            catch(Exception e)
            {
                Log.e("hsh", e.getMessage());
            }
        }
    }
    //*/   
    
}
