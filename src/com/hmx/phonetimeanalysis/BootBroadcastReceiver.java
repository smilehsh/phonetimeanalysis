package com.hmx.phonetimeanalysis;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class BootBroadcastReceiver extends BroadcastReceiver {

    static final String action_boot="android.intent.action.BOOT_COMPLETED"; 
    static final String action_present= "android.intent.action.USER_PRESENT";
    @Override

    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(action_boot)){ 
            Intent sIntent =new Intent(context, AnalysisService.class);
            Bundle bundle1 = new Bundle();
            int status = 1;
            bundle1.putInt("CMD_TYPE", status);
            sIntent.putExtras(bundle1);
            context.startService(sIntent) ; 
        }
        else if(intent.getAction().equals(action_present)){ 
            Intent sIntent =new Intent(context, AnalysisService.class);
            Bundle bundle1 = new Bundle();
            int status = 2;
            bundle1.putInt("CMD_TYPE", status);
            sIntent.putExtras(bundle1);
            context.startService(sIntent) ; 
        }
    }

 

}
