package com.hmx.phonetimeanalysis;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;




import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

public class Tools {
    public static boolean mDebug = true;
    private static String mFilterTAG = "";  

	public static final String PREFERENCES_NAME = "MESSAGE_CONTROLLER_PREFERENCES";
	public static final String KEY_MESSAGE_MARK = "CONTROL_MESSAGE_MARK";
	public static final String KEY_NIGHT_MARK   = "CONTROL_NIGHT_MARK";
	public static final String KEY_VIBRATE_MARK = "CONTROL_VIBRATE_MARK";
	public static final String KEY_VOICE_MARK  = "CONTROL_VOICE_MARK";
	
	public static final String	SHARE_RESULT_TITLE_STR  = "SHARE_RESULT_TITLE_STR";
	public static final String	SHARE_RESULT_CONTENT_STR  = "SHARE_RESULT_CONTENT_STR";
	private static String mPeerId = null;
	private static String mOSVersion = null;
//	private static String mSelfAppVersion = null;
	private static String mIMEI = null;
	private static String mIMEIEX = null;
	public final static byte[] lock = new byte[0]; // 特殊的instance变量
	public static void Logd(String tag,String message)
	{
	        if(mDebug == false)
	            return;
	        if(mFilterTAG.equals("") || mFilterTAG.equals(tag))
	        {
	            Log.d(tag, message);
	        }
	}
	public static synchronized long getPreferences(Context ctx,String key) {
		SharedPreferences sp = ctx.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		long value = sp.getLong(key, 0);
        return value;
	}
	
	public static synchronized String getPreferencesString(Context ctx,String key,String sdefault) {
		SharedPreferences sp = ctx.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		String value = sp.getString(key,sdefault);
        return value;
	}
	
	/**
	 * –Ú¡�?ªØ∂�?œÛŒ™String◊÷∑˚¥Æ
	 * 
	 * @param o
	 *            Object
	 * @return String
	 * @throws Exception
	 */
	public static String writeObject(Object o) throws Exception {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(o);
		oos.flush();
		oos.close();
		bos.close();
		return Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
	}

	/**
	 * ∑¥�?Ú¡–ªØ◊÷∑˚¥ÆŒ™∂�?œÛ
	 * 
	 * @param object
	 * @return
	 * @throws Exception
	 */
	public static synchronized Object readObject(String object) throws Exception {
		ByteArrayInputStream bis = new ByteArrayInputStream(
				Base64.decode(object, Base64.DEFAULT));
		ObjectInputStream ois = new ObjectInputStream(bis);
		Object o = ois.readObject();
		bis.close();
		ois.close();
		return o;
	}
	public static synchronized void setObject(Context ctx,String id, Object o) {
		
		SharedPreferences mPreferences = ctx.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = mPreferences.edit();
		String str = null;
		try {
			str = writeObject(o);
		} catch (Exception e) {

		}
		editor.putString(id, str);
		editor.commit();
	}

	public static synchronized Object getObject(Context ctx,String id, Object defaultValue) {
		Object o = defaultValue;
		SharedPreferences mPreferences = ctx.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		
		String str = mPreferences.getString(id, null);
		if(str != null){
			try {
				o = readObject(str);
			} catch (Exception e) {
				// e.printStackTrace();

			}
		}
		return o;
	}
	
	public static synchronized void setPreferencesString(Context ctx,String key,String value) {
		
		SharedPreferences sp = ctx.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}	
	
	public static synchronized void setPreferences(Context ctx,String key, long value) {
		
		SharedPreferences sp = ctx.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sp.edit();
		editor.putLong(key, value);
		editor.commit();
	}
		public Tools() {
		// TODO Auto-generated constructor stub
		}
		public static String getSDCardDir() {
			// return Environment.getExternalStorageDirectory().getPath();
			String sdcardPath = Environment.getExternalStorageDirectory().getPath();
			if (null != sdcardPath && !sdcardPath.endsWith("/")) {
				sdcardPath = sdcardPath + "/";
			}
			return sdcardPath;
		}
		private static void CmdExec(String cmd) throws IOException, InterruptedException{
		 Process proc = Runtime.getRuntime().exec(cmd);
		 Log.e("hsh", "Runtime.getRuntime ="+proc);
	     StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "Error");            
	     StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "Output");
	     errorGobbler.start();
	     outputGobbler.start();
	     proc.waitFor();
		}
		public static void chmod(String permission, String path) {
		String command = "chmod " + permission + " " + path;
		try {
			CmdExec(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
		static class StreamGobbler extends Thread {
			 InputStream is;
			 String type;
			 StreamGobbler(InputStream is, String type) {
			  this.is = is;
			  this.type = type;
			 }
			 public void run() {
			  try {
				  
			   InputStreamReader isr = new InputStreamReader(is);
			   BufferedReader br = new BufferedReader(isr);
			   String line = null;
			   while ((line = br.readLine()) != null) {
			   }
			  } catch (IOException ioe) {
			   ioe.printStackTrace();
			  }
			 }
			}
		// ��ȡpeerid
		public static String getPeerid(final Context context) {
			if (null == mPeerId && null != context) {
				WifiManager wm = (WifiManager) context
						.getSystemService(Context.WIFI_SERVICE);
				if (null != wm && null != wm.getConnectionInfo()) {
					String mac = wm.getConnectionInfo().getMacAddress();
					mac += "004V";
					mPeerId = mac.replaceAll(":", "");
					mPeerId = mPeerId.replaceAll(",", "");
					mPeerId = mPeerId.replaceAll("[.]", "");
					mPeerId = mPeerId.toUpperCase();
				}
			}
	
			return mPeerId;
		}

		// ��ȡϵͳ�汾��Ϣ
		public static String getOSVersion() {
			if (null == mOSVersion) {
				mOSVersion = "SDKV = " + android.os.Build.VERSION.RELEASE;
				mOSVersion += "_MANUFACTURER = " + android.os.Build.MANUFACTURER;
				mOSVersion += "_MODEL = " + android.os.Build.MODEL;
				mOSVersion += "_PRODUCT = " + android.os.Build.PRODUCT;
				mOSVersion += "_FINGERPRINT = " + android.os.Build.FINGERPRINT;
				mOSVersion += "_CPU_ABI = " + android.os.Build.CPU_ABI;
				mOSVersion += "_ID = " + android.os.Build.ID;
			}
			return mOSVersion;
		}
	    public static String getVersionName(Context ctx){
	    	if (ctx!=null) {
				//return ctx.getResources().getString("");
			}
	    	return "";
	    }
		public static String getSelfAppVersion(final Context context) {

			return getVersionName(context);
		}

		// ��ȡIMEIֵ
		public static String getIMEI(final Context context) {
			if (null == mIMEI && null != context) {
				TelephonyManager tm = (TelephonyManager) context
						.getSystemService(Context.TELEPHONY_SERVICE);
				if (null != tm) {
					mIMEI = tm.getDeviceId();
				}
				if (null == mIMEI) {
					  WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);  
	                  try
	                  {
    					  WifiInfo info = wifi.getConnectionInfo();  
    				      mIMEI = info.getMacAddress();
	                    }
                      catch(Exception e)
                      {
                          mIMEI = "000000000000000";
                      }
					//mIMEI = "000000000000000";
				}
			}
			//return "355666058558679";
			return mIMEI;
		}
		public static String getIMEIEX(final Context context) {
			if (null == mIMEIEX && null != context) {
				TelephonyManager tm = (TelephonyManager) context
						.getSystemService(Context.TELEPHONY_SERVICE);
				if (null != tm) {
					mIMEIEX = tm.getDeviceId();
				}
				if (null == mIMEIEX) {
					  WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);  
				      try
				      {
    					  WifiInfo info = wifi.getConnectionInfo();  
    				      mIMEIEX = info.getMacAddress();
    				      mIMEIEX = mIMEIEX.replaceAll(":", "");
    				      mIMEIEX = mIMEIEX.replaceAll(",", "");
    				      mIMEIEX = mIMEIEX.replaceAll("[.]", "");
				      }
				      catch(Exception e)
				      {
				          mIMEIEX = "000000000000000";
				      }
				}
			}
			return mIMEIEX;
		}
		
		public static long getNextDayStart(long msec)
		{
		    long ret = -1;
		    Calendar curDate = Calendar.getInstance();
            curDate.setTimeInMillis(msec);
            curDate.set(Calendar.HOUR_OF_DAY, 0);
            curDate.set(Calendar.MINUTE, 0);
            curDate.set(Calendar.SECOND, 0);
            curDate.set(Calendar.MILLISECOND, 0);
            curDate.add(Calendar.DATE, 1);
            ret = curDate.getTimeInMillis();
		    return ret;
		}
		public static String covertTimeToStr(long sec)
		{
		    String str = "";
		    int x = (int)(sec/3600);
		    int y = (int)(sec%3600);
		    int z = y/60;
		    int k = y%60;
		    str = String.format("%02d:%02d:%02d", x,z,k);
		    return str;
		}
	    public static String getCurDay() { 
	        String str = ""; 
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
	 
	        Calendar lastDate = Calendar.getInstance(); 
	        str = sdf.format(lastDate.getTime()); 
	        return str; 
	    }   

	    public static String getCurMonth() { 
	        String str = ""; 
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM"); 
	 
	        Calendar lastDate = Calendar.getInstance(); 
	        str = sdf.format(lastDate.getTime()); 
	        return str; 
	    }       
	    public static String getDefaultDay() { 
	            String str = ""; 
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
	            Calendar lastDate = Calendar.getInstance(); 
	            lastDate.set(Calendar.DATE, 1);
	            lastDate.add(Calendar.MONTH, 1); 
	            lastDate.add(Calendar.DATE, -1); 
	     
	            str = sdf.format(lastDate.getTime()); 
	            return str; 
	     }  
	    public static String getAppNameByPkg(Context ctx, String pkg)
	    {
	        String Name = ApplicationUtil.getProgramNameByPackageName(ctx, pkg);
	        return Name;
	    }	    
}
