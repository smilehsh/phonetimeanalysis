package com.hmx.phonetimeanalysis;


import java.util.Iterator;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class HelpActivity extends Activity{
    private TextView InfoText;
    @Override
    public void onResume() {
        super.onResume();
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_app);
        InfoText = (TextView)findViewById(R.id.infoText);
        HmxApplication capp = (HmxApplication) getApplication();
        Map<String,String> infomap = capp.GetMap();
        String Info = "";
//        Iterator<String> it=infomap.keySet().iterator(); 
//        while(it.hasNext()){ 
//            String key; 
//            String value; 
//            key=it.next().toString(); 
//            value=infomap.get(key); 
//            long sec =  Long.parseLong(value);
//            Info = Info+ this.getResources().getString(R.string.detail_title1) + key +  this.getResources().getString(R.string.detail_title2) +Tools.covertTimeToStr(sec) + "\n";
//         }
        Info = Info+ this.getResources().getString(R.string.help_content);
        InfoText.setText(Info);
    }

}
