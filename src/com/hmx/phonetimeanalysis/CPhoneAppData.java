package com.hmx.phonetimeanalysis;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CPhoneAppData {
    private static CPhoneAppData  sinstance = null;
    private Context mctx;
    public CPhoneAppData (Context ctx)
    {
        mctx = ctx;
    }
    
    public synchronized static CPhoneAppData getInstance(Context ctx)
    {
        if(sinstance == null)
        {
            sinstance = new CPhoneAppData(ctx);
        }
        return sinstance;
    }
    //palyer report   
    public void saveCPacketStrcutToDb(CPacketStrcut struct)
    {
        if(isInTbByCPacketStrcut(struct) == false)
        {
            insertCPacketStrcutToDb(struct);
        }
    }    
    
    public void insertCPacketStrcutToDb(CPacketStrcut struct)
    {
        synchronized(Tools.lock) 
        {
            SQLiteHelper sh = new SQLiteHelper(mctx, "adsc_db");
            SQLiteDatabase db = sh.getWritableDatabase();
            ContentValues values = new ContentValues();
            //values.put("tpl_id", report.tpl_id);

            db.insert("player_report_list", null, values); 
            db.close();
        }
    } 
 
    //check file is in db
    public boolean isInTbByCPacketStrcut(CPacketStrcut struct)
    {
        boolean rtn = false;
        synchronized(Tools.lock) 
        {
            Cursor cursor = null; 
            SQLiteDatabase db = null;
            try
            {
                SQLiteHelper sh = new SQLiteHelper(mctx, "adsc_db");
                db = sh.getReadableDatabase();        
                //cursor = db.rawQuery("select * from player_report_list where tpl_id = ? and start = ? and end = ?", new String[]{report.tpl_id+"",report.start + "",report.end + ""});
                int count =  cursor.getCount();
                if(cursor != null && count > 0)
                {   
                    while(cursor.moveToNext()){
                        rtn = true;
                        break;
                    }
                }
            }catch(Exception e){
                
            }finally{
                if(cursor != null)
                {
                    cursor.close();
                    cursor = null;
                }
                if(db != null)
                {
                    db.close();
                }
            }
        }
        return  rtn;
    }        

}
