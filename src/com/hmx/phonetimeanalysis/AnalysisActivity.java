package com.hmx.phonetimeanalysis;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import com.hmx.phonetimeanalysis.AnalysisService;
import com.hmx.phonetimeanalysis.AnalysisService.HmxBinder;
import com.hmx.chart.BarChartView;
import com.hmx.guide.GuideFlipView;
import com.umeng.analytics.MobclickAgent;
import com.umeng.fb.FeedbackAgent;
import com.umeng.update.UmengUpdateAgent;

public class AnalysisActivity extends Activity implements OnClickListener {
    private Button barButton;
    private Button pieButton;  
    private Button helpButton;    
    private AnalysisService mService;
    private Intent svrIntent;
    private Context mContext;
    private LinearLayout mbarlinear;
    private BarChartView mbarView; 
    
    private final  String mPageName = "AnalysisActivity_Start";
    private Handler UiHandler   = new Handler()
    {
           public void handleMessage(Message msg)
           {
                  switch (msg.what)
                  {
                  case 1:
                         HmxApplication app = (HmxApplication) getApplication();
                         app.clearInfoData();
                         List<String> options = Collections.synchronizedList(new ArrayList<String>());
                         options.add("");
                         List<String> values = Collections.synchronizedList(new ArrayList<String>());
                         if(mService != null)
                         {
                             long totallen = 0;
                             Map<String,String> map =  mService.getPkgTimeMap();
                             if(map == null)
                             {
                                 return;
                             }
                             Iterator<String> it=map.keySet().iterator(); 
                             long max = 100;
                             while(it.hasNext()){ 
                                 String key; 
                                 String value; 
                                 key=it.next().toString(); 
                                 value=map.get(key); 
                                 String Name = ApplicationUtil.getProgramNameByPackageName(AnalysisActivity.this, key);
                                 long havelen = 0;
                                 if(value != null)
                                 {
                                     try
                                     {
                                         havelen = Long.parseLong(value);
                                     }
                                     catch(Exception e)
                                     {
                                         
                                     }
                                 }
                                 totallen += havelen/1000;
                                 max = havelen/1000 >= max? havelen/1000 + 100:max;
                                 app.addInfo(Name, havelen/1000 + "");
                                 if(Name.length()> 4)
                                 {
                                     Name = Name.substring(0, 4) + ".";
                                 }
                                 options.add(Name);
                                 values.add(havelen/1000 + "");
                             }
                             int lsize = values.size();
                             if(lsize > 0)
                             {
                                 double[] first = new double[lsize];
                                 double[] second = new double[lsize];
                                 for(int i =0; i<lsize;i++)
                                 {
                                     first[i] =Double.valueOf(values.get(i));
                                     second[i] =Double.valueOf(values.get(i));
                                 }
                                 mbarlinear.removeAllViews();  
                                 mbarView = new BarChartView(AnalysisActivity.this, true);
                                 mbarView.setTitles(AnalysisActivity.this.getResources().getString(R.string.app_name));
                                 mbarView.setYMax(Double.valueOf(max+""));
                                 mbarView.initData(first, second, options, AnalysisActivity.this.getResources().getString(R.string.app_name));  
                                 mbarlinear.setBackgroundColor(0xFFB0E2FF);  
                                 mbarlinear.addView(mbarView.getBarChartView());   
                             }
                             app.setTotalTimer(totallen);
                         }
                         break;
                  }
                  super.handleMessage(msg);
           }                  
    };
    private ServiceConnection mConn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((HmxBinder)service).getService();
    }

    public void onServiceDisconnected(ComponentName name) {
            mService = null;
    }
    
    };
    public void SafeSleepTime(int microsec)
    {
        try {
            Thread.sleep(microsec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart( mPageName );
        mPageName.hashCode();
        MobclickAgent.onResume(mContext);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd( mPageName );
        MobclickAgent.onPause(mContext);
    }    
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
      switch (resultCode)
      { 
        case 0:
            break;
        default:
            break;
      } 
    }          
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.analysis_activity_main);
        long first =  Tools.getPreferences(AnalysisActivity.this,Tools.KEY_NIGHT_MARK);
        if(first == -1)
        {
            Tools.setPreferences(AnalysisActivity.this,Tools.KEY_NIGHT_MARK,1);
            Intent loadIntent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("mark",0);                  
            loadIntent.putExtras(bundle);
            loadIntent.setClass(this, GuideFlipView.class);         
            startActivityForResult(loadIntent,0);            
        }
        svrIntent =new Intent(this,AnalysisService.class);
        barButton = (Button) findViewById(R.id.bar);
        pieButton = (Button) findViewById(R.id.pie);
        helpButton = (Button) findViewById(R.id.help);
        mbarlinear = (LinearLayout) findViewById(R.id.barview_content);  
        barButton.setOnClickListener(this);
        pieButton.setOnClickListener(this);
        helpButton.setOnClickListener(this);
        mContext = this;    
        MobclickAgent.setDebugMode(true);
        Bundle bundle = new Bundle();
        int status = 2;
        bundle.putInt("CMD_TYPE", status);
        svrIntent.putExtras(bundle);
        startService(svrIntent);
        bindService(svrIntent, mConn, 0); 
        Thread mThread = new Thread(new Runnable(){
            public void run() {
                long index = 0;
                while(true)
                {
                    index++;
                    if(index % 3 ==0)
                    {
                        index = 0;
                        UiHandler.sendEmptyMessage(1);
                    }
                    SafeSleepTime(1100);
                }
                
            }
        }); 
        mThread.setPriority(10);
        mThread.start();   
        FeedbackAgent agent = new FeedbackAgent(this);
        agent.sync();
        UmengUpdateAgent.update(AnalysisActivity.this);  
//        AdView mAdView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
//        /*初始化admob*/
//        AdmobInst.GetInstance().Init(30000, 
//                                    this, "ca-app-pub-4955754913688823/8410031593");
//        
//        /*显示admob*/
//        AdmobInst.GetInstance().ShowAdmob();        

    }
    


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
        case R.id.bar:
            Intent firstIntent = new Intent();
            firstIntent.setClass(AnalysisActivity.this, DetailActivity.class);           
            startActivity(firstIntent);
            break;
        case R.id.pie:
            FeedbackAgent agent = new FeedbackAgent(this);
            agent.startFeedbackActivity();
            break;
        case R.id.help:
            Intent secondIntent = new Intent();
            secondIntent.setClass(AnalysisActivity.this, HelpActivity.class);           
            startActivity(secondIntent);
            break;
        }
    }

}
