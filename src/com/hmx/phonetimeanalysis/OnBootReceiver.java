/***
  Copyright (c) 2008-2012 CommonsWare, LLC
  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
  
  From _The Busy Coder's Guide to Advanced Android Development_
    http://commonsware.com/AdvAndroid
*/

package com.hmx.phonetimeanalysis;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

public class OnBootReceiver extends BroadcastReceiver {
  private static final int PERIOD=5000;  // 5 minutes
  static final String action_boot="android.intent.action.BOOT_COMPLETED"; 
  @Override
  public void onReceive(Context context, Intent intent) {
      
      Log.e("hsh", "hsh receive boot complete");  
      if (intent.getAction().equals(action_boot)){ 
          Log.e("hsh", "hsh receive boot complete"); 
          Intent sIntent =new Intent(context, AnalysisService.class);
          Bundle bundle1 = new Bundle();
          int status = 1;
          bundle1.putInt("CMD_TYPE", status);
          sIntent.putExtras(bundle1);
          context.startService(sIntent) ; 
      }
     /* 
    AlarmManager mgr=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
    Intent i=new Intent(context, OnAlarmReceiver.class);
    PendingIntent pi=PendingIntent.getBroadcast(context, 0,
                                              i, 0);
    
    mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                      SystemClock.elapsedRealtime()+60000,
                      PERIOD,
                      pi);
                      
   //*/                   
  }
}