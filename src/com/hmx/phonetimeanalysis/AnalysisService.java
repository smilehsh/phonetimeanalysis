package com.hmx.phonetimeanalysis;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.hmx.phonetimeanalysis.ScreenObserver.ScreenStateListener;
import com.umeng.analytics.MobclickAgent;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class AnalysisService extends Service{
	private final String TAG = "AnalysisService";
    private HmxBinder mBinder = new HmxBinder();
	private Map<String,String> pkgTimeMap = new HashMap<String,String>();
	private String curpkg = null;
	private long curTime = 0;
	private ScreenObserver mScreenObserver;  
	private boolean mIsScreenOn = false;
	public final static byte[] slock = new byte[0];
	public final String TB_PRE = "tb";
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return (IBinder) mBinder;
	}
	public Map<String,String> getPkgTimeMap()
	{
	    return pkgTimeMap;
	}
	public void printMapData()
	{
        Iterator<String> it=pkgTimeMap.keySet().iterator(); 
        while(it.hasNext()){ 
            String key; 
            String value; 
            key=it.next().toString(); 
            value=pkgTimeMap.get(key); 
            Tools.Logd(TAG, "hsh pkg time use  pkg name :"+ key + " and use time = " + value);
        }
	}

	public void updataItem(String pkg,String cdate,String time,String month)
	{
        synchronized(slock) 
        {
            CSqlInstance sInstance = CSqlInstance.getInstance(this);
            try
            {      
                String sql = "update "+month + " set len = '" +  time +"' where pkgname like '"  +pkg +  "' and cdate like '" + cdate +"'";//�޸ĵ�SQL���
                sInstance.getSqlDatabase().execSQL(sql);
            }catch(Exception e)
            {
                Tools.Logd(TAG, e.getMessage());
            }
        }
	}
    public void insertItem(String pkg,String cdate,String time,String month)
    {
        synchronized(slock) 
        {
            try
            {            
                CSqlInstance sInstance = CSqlInstance.getInstance(this);
                ContentValues values = new ContentValues();
                values.put("pkgname", pkg);
                values.put("cdate", cdate);
                values.put("appname", Tools.getAppNameByPkg(AnalysisService.this, pkg));
                values.put("len", time);
                sInstance.getSqlDatabase().insert(month, null, values); 
            }catch(Exception e)
            {
                Tools.Logd(TAG, e.getMessage());
            }
        }
    }	
    public boolean getItemExsit(String pkg,String cdate,String month)
    {
        CSqlInstance sInstance = CSqlInstance.getInstance(this);

        boolean ret = false;
        synchronized(slock) 
        {
           Cursor cursor = null;
           try
           {
               cursor  =  sInstance.getSqlDatabase().query(month, new String[]{"pkgname","cdate","appname","len"}, "pkgname like ? and cdate like ?", new String[]{pkg,cdate}, "", "", "");
               int count  = cursor.getCount();
               if(cursor != null &&  count> 0)
               {   
                   while(cursor.moveToNext()){
                           ret = true;
                           break;
                       }
                   }

               }catch(Exception e)
               {
                   Tools.Logd(TAG, e.getMessage());
               }
               finally
               {
                   if(cursor != null)
                   {
                       cursor.close();
                       cursor = null;
                   }
               }
        }
        return ret;
    }	
    public void saveMapDataToDb()
    {
        Iterator<String> it=pkgTimeMap.keySet().iterator(); 
        while(it.hasNext()){ 
            String key; 
            String value; 
            key=it.next().toString(); 
            value=pkgTimeMap.get(key);
            CSqlInstance sInstance = CSqlInstance.getInstance(this);
            String month = sInstance.getCurMonthTb();
            if(getItemExsit(key,Tools.getCurDay(),month))
            {
                updataItem(key,Tools.getCurDay(),value,month);
            }
            else
            {
                insertItem(key,Tools.getCurDay(),value,month);
            }
            
        }
  
    }

    
        
    public void LoadMapDataFromDb()
    {
        CSqlInstance sInstance = CSqlInstance.getInstance(this);
        String month = sInstance.getCurMonthTb();
        if(month.equals(""))
        {
            Tools.Logd(TAG, "month ==   (NULL) ");
        }
        
        synchronized(slock) 
        {
           Cursor cursor = null;
           try
           {
               cursor  =  sInstance.getSqlDatabase().query(month, new String[]{"pkgname","cdate","appname","len"}, " cdate like ?", new String[]{Tools.getCurDay()}, "", "", "");
               int count  = cursor.getCount();
               if(cursor != null &&  count> 0)
               {   
                   while(cursor.moveToNext()){
                           String key  = cursor.getString(cursor.getColumnIndex("pkgname"));
                           String value = cursor.getLong(cursor.getColumnIndex("len"))+"";
                           pkgTimeMap.put(key, value);
                       }
               }

               }catch(Exception e)
               {
                   Tools.Logd(TAG, e.getMessage());
               }
               finally
               {
                   if(cursor != null)
                   {
                       cursor.close();
                       cursor = null;
                   }
               }

        }        
        Iterator<String> it=pkgTimeMap.keySet().iterator(); 
        while(it.hasNext()){ 
            String key; 
            String value; 
            key=it.next().toString(); 
            value=pkgTimeMap.get(key); 
            Tools.Logd(TAG, "hsh pkg time use  pkg name :"+ key + " and use time = " + value);
        }
  
    }    

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub

		super.onCreate();	
	    MobclickAgent.setDebugMode(true);
	    MobclickAgent.updateOnlineConfig(this);
        mScreenObserver = new ScreenObserver(this);  
        mScreenObserver.requestScreenStateUpdate(new ScreenStateListener() {  
            @Override  
            public void onScreenOn() {  
                //doSomethingOnScreenOn();  
                mIsScreenOn = true;
            }  
            @Override  
            public void onScreenOff() {  
                //doSomethingOnScreenOff();
                mIsScreenOn = false;
            }  
        });  
  		Thread mThread = new Thread(new Runnable(){
				public void run() {
				    curTime = System.currentTimeMillis();
				    LoadMapDataFromDb();
				    long index = 0;
				    long startTime = curTime;
				    while(true)
				    {
				        index++;
				        if(!mIsScreenOn)
				        {
				            curTime = System.currentTimeMillis();
				            continue;
				        }
				        if((index % 5) == 0)
				        {
				            printMapData();
				        }
                        if(index == 10)
                        {
                            index = 0;
                            saveMapDataToDb();
                            //start one day end or next month cacal
                            long ncurtime = System.currentTimeMillis();
                            long nextTime = Tools.getNextDayStart(startTime);
                            if(ncurtime - nextTime >= 0)
                            {
                                pkgTimeMap.clear();
                                pkgTimeMap = null;
                                pkgTimeMap = new HashMap<String,String>();
                                startTime = ncurtime;
                                curTime = ncurtime;
                                
                            }
                        }				        
				        long sec = System.currentTimeMillis();
				        long len  = 0;
				        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
				        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				        //if(curpkg == null || curpkg.equals(cn.getPackageName()) == false)
				        {
				            curpkg = cn.getPackageName();
				            len = sec -curTime;
				            curTime = sec;
				            String pkgLen = pkgTimeMap.get(curpkg);
				            long havelen = 0;
				            if(pkgLen != null)
				            {
				                try
				                {
				                    havelen = Long.parseLong(pkgLen);
				                }
				                catch(Exception e)
				                {
				                }
				            }
				            String newlen = (len + havelen) +"";
				            pkgTimeMap.put(curpkg, newlen);
				        }
				        Tools.Logd(TAG, "hsh pkg:"+cn.getPackageName());
				        Tools.Logd(TAG, "hsh cls:"+cn.getClassName()); 
				        SafeSleepTime(1100);
				    }
				    
				}
		    }); 
  		mThread.setPriority(10);
  		//mThread.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
  		mThread.start(); 
	}

	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
        //ֹͣ����screen״̬  
        mScreenObserver.stopScreenStateUpdate();
        MobclickAgent.updateOnlineConfig(this);
		
	}
	@Override  
    public boolean onUnbind(Intent intent) {  
        // All clients have unbound with unbindService()   
        return true;  
    }
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		// TODO Auto-generated method stub		
		if(intent == null)
			return super.onStartCommand(intent, flags, startId);	
		int mPlayerMsg = (int)intent.getIntExtra("CMD_TYPE", 0);
    	switch(mPlayerMsg)
    	{
    	case 0: 	
    		break;
    	case 1:
    		break;
    	case 2:
    		break;
    	}
    	return START_STICKY;
		//return super.onStartCommand(intent, flags, startId);
	}

    public class HmxBinder extends Binder {
		
		public AnalysisService getService() {
			return AnalysisService.this;
		}
	}
	public void SafeSleepTime(int microsec)
	{
		try {
			Thread.sleep(microsec);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
