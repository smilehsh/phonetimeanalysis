package com.hmx.phonetimeanalysis;


import java.util.Iterator;
import java.util.Map;

import com.umeng.fb.FeedbackAgent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends Activity implements OnClickListener {
    private static String TAG = "DetailActivity";
    private TextView InfoText;
    private long curMills = 0;
    private String curDay = null;
    private String curMonth = null;
    private Button leftButton;
    private Button rightButton; 
    @Override
    public void onResume() {
        super.onResume();
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_app);
        InfoText = (TextView)findViewById(R.id.infoText);
        leftButton = (Button) findViewById(R.id.btn_left);
        rightButton = (Button) findViewById(R.id.btn_right);
        leftButton.setOnClickListener(this);
        rightButton.setOnClickListener(this);
        curMills = System.currentTimeMillis();
        curDay = Tools.getCurDay();
        curMonth = Tools.getCurMonth();
        loadDatafromDb(curDay,curMonth);
        
        
        /*
        Map<String,String> infomap = capp.GetMap();
        String Info = "";
        Iterator<String> it=infomap.keySet().iterator(); 
        while(it.hasNext()){ 
            String key; 
            String value; 
            key=it.next().toString(); 
            value=infomap.get(key); 
            long sec =  Long.parseLong(value);
            Info = Info+ this.getResources().getString(R.string.detail_title1) + key +  this.getResources().getString(R.string.detail_title2) +Tools.covertTimeToStr(sec) + "\n";
         }
        InfoText.setText(Info);
        //*/
    }
    
    public void loadDatafromDb(String day,String month)
    {
        Map<String,String> infomap = CSqlInstance.getInstance(this).getAppInfo(day, month);
        String Info = "";
        Iterator<String> it=infomap.keySet().iterator(); 
        while(it.hasNext()){ 
            String key; 
            String value; 
            key=it.next().toString(); 
            value=infomap.get(key); 
            try
            {
                long sec =  Long.parseLong(value)/1000;
                Info = Info+ this.getResources().getString(R.string.detail_title1) + key +  this.getResources().getString(R.string.detail_title2) +Tools.covertTimeToStr(sec) + "\n";
            }catch(Exception e)
            {
                Tools.Logd(TAG, e.getMessage());
            }
         }
        InfoText.setText(Info);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
        case R.id.btn_left:
            
            break;
        case R.id.btn_right:
            loadDatafromDb(curDay,curMonth);
            break;
        default:
            break;
        }
    }
}
