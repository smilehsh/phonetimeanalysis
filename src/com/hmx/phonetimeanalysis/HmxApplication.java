package com.hmx.phonetimeanalysis;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

public class HmxApplication extends Application {
    private Map<String,String> infomap = new ConcurrentHashMap<String,String>();
    private long totalUseTime = 0;
    private SQLiteHelper msqlhelper = null;
    private SQLiteDatabase mwdb = null;     
    @Override
    public void onCreate() {
        super.onCreate();
        msqlhelper = new SQLiteHelper(getApplicationContext(), "apaa_db");
        mwdb = msqlhelper.getWritableDatabase();     
        CSqlInstance.getInstance(getApplicationContext());
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());
    }
    public void addInfo(String key,String value)
    {
        infomap.put(key, value);
    }
    public Map<String,String> GetMap()
    {
        return infomap;
    }
    public void clearInfoData()
    {
        infomap.clear();
        infomap = null;
        infomap = new HashMap<String,String>();
    }
    public void setTotalTimer(long time)
    {
        totalUseTime = time;
    }    
    public long getTotalTimer()
    {
        return totalUseTime;
    }
    public SQLiteDatabase getWritableDatabase()
    {
        return mwdb;
    }   
    public void releaseDb()
    {
        if(mwdb != null)
            mwdb.close();
    }
    public void resetDb()
    {
        if(msqlhelper == null || mwdb == null)
            resetDabase();
        if(msqlhelper != null && mwdb != null)
        {
            msqlhelper.resetDbTable(mwdb);
        }
    }
    public void resetDabase()
    {
        
        if(msqlhelper != null)
        {
            if(mwdb != null)
            {
                mwdb.close();
                mwdb = null;
            }
            msqlhelper.close();
            msqlhelper = null;
        }
        msqlhelper = new SQLiteHelper(getApplicationContext(), "apaa_db");
        mwdb = msqlhelper.getWritableDatabase();
    }
    
    
    
}
