package com.hmx.phonetimeanalysis;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class SQLiteHelper extends SQLiteOpenHelper{
	private final static int VERSION = 2;
	public final static String TAG = "SQLiteHelper";
	public SQLiteHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	public SQLiteHelper(Context context, String name) {
		super(context, name, null, VERSION);
	}
	
    @Override
    public void onCreate(SQLiteDatabase db) {
        onUpgrade(db,0,VERSION);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for(int version = oldVersion+1;version <= newVersion ;version++)
        {
            upgradeTo(db,version);
        }
    }
    private void upgradeTo(SQLiteDatabase db,int version)
    {
        switch(version)
        {
            case 1:
                db.execSQL("create table PhoneTimesList"+ Tools.getCurMonth()+"(id integer primary key autoincrement, pkgname varchar(1024) , appname varchar(256),cdate varchar(256),len long,bak int)");  
                break;
            case 2:
                db.execSQL("create table tbMonth(id integer primary key autoincrement, month varchar(1024))");  
                break;
            default:
                break;
        }
    }

    private void DownTo(SQLiteDatabase db,int version)
    {
        switch(version)
        {
            case 1:
                db.execSQL("DROP TABLE IF EXISTS " + "playlist");
                break;

             default:
                break;
        }
    } 
    
    public void deleteTable(SQLiteDatabase db,String table)
    {
        try
        {
            db.execSQL("DROP TABLE IF EXISTS " + "playlist"); 
        }catch(Exception e)
        {
           Tools.Logd(TAG, e.getMessage());
        }
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        //super.onDowngrade(db, oldVersion, newVersion);
        for(int version = oldVersion-1;version >= newVersion ;version--)
        {
            DownTo(db,version);
        }
    }
    /*
    private void addColumn(SQLiteDatabase db, String dbTable, String columnName,
            String columnDefinition) {
            db.execSQL("ALTER TABLE " + dbTable + " ADD COLUMN " + columnName + " "
                + columnDefinition);
    }
    //*/
    public void resetDbTable(SQLiteDatabase db)
    {
        onDowngrade(db,VERSION,0);
        onUpgrade(db,0,VERSION);
    }
}
