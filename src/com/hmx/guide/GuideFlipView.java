package com.hmx.guide;


import com.hmx.phonetimeanalysis.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnFocusChangeListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class GuideFlipView extends Activity implements OnGestureListener {

	private ViewFlipper flipper;
	private GestureDetector detector;
	private int indexView = 0;
	private View iv1 = null;
	private View iv2 = null;
	private View iv3 = null;
	Intent intent;
	Bundle bunde;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_flip_view);	
	    /* ȡ��Intent�е�Bundle���� */
	    intent=this.getIntent();
	    bunde = intent.getExtras();
	    
		detector = new GestureDetector(this);
		flipper = (ViewFlipper) this.findViewById(R.id.ViewFlipper01);
		iv1 = addImageView(R.drawable.flip_1);
		iv2 = addImageView(R.drawable.flip_2);
		iv3 = addImageView(R.drawable.flip_3);
		flipper.addView(iv1);
		flipper.addView(iv2);
		flipper.addView(iv3);
		flipper.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				switch (v.getId()) {
				case 1:
					break;
				case 2:
					break;
				case 3:
					break;
				default:
					break;
				}

			}
		});
	}

	private View addTextView(String text) {
		TextView tv = new TextView(this);
		tv.setText(text);
		tv.setGravity(1);
		return tv;
	}
	private View addImageView(int id) {
		 
		ImageView tv = new ImageView(this);
		tv.setBackgroundDrawable(getResources().getDrawable(id));//R.drawable.flip_1

		return tv;
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return this.detector.onTouchEvent(event);
	}

	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		if (e1.getX() - e2.getX() > 100 ) {
			View  tiv = this.flipper.getCurrentView();
			if(tiv.equals(iv3))
			{
				this.setResult(0, intent);
				finish();
			}
			this.flipper.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_in));
			this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_out));
			this.flipper.showNext();
			
			return true;
		} else if (e1.getX() - e2.getX() < -100) {
			View  tiv = this.flipper.getCurrentView();
			if(tiv.equals(iv1))
				return false;
			this.flipper.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_in));
			this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_out));
			
			this.flipper.showPrevious();
			return true;
		}		
			
		return false;
	}

	public void onLongPress(MotionEvent e) {

	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}
}