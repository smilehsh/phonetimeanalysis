package com.hmx.chart;

import java.util.Iterator;
import java.util.Map;

import org.achartengine.ChartFactory;

import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

import com.hmx.phonetimeanalysis.HmxApplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;

/**
 * 柱状图
 * 
 * @Package com.manyou.androidchart
 * @FileName BarChart.java
 * @Author APKBUS-manyou
 * @Date 2013-1-30
 */
public class BarChart implements AChartAbstract {
    HmxApplication capp;
	public Intent getIntent(Context context,HmxApplication app) {
	    capp = app;
		return ChartFactory.getBarChartIntent(context, getDataSet(), getRenderer(), Type.STACKED, "应用时间使用比例");
	}

	/**
	 * 构造数据
	 * @return
	 */
	public XYMultipleSeriesDataset getDataSet() {
		// 构造数据
		XYMultipleSeriesDataset barDataset = new XYMultipleSeriesDataset();
		CategorySeries barSeries = new CategorySeries("应用时间使用比例");
		//barSeries.add(43.1);
        Map<String,String> infomap = capp.GetMap();
        if(infomap!= null && infomap.size() > 0)
        {
            Iterator<String> it=infomap.keySet().iterator(); 
            while(it.hasNext()){ 
                String key; 
                String value; 
                key=it.next().toString(); 
                value=infomap.get(key); 
                long apptime = Long.parseLong(value);
                barSeries.add(key, apptime);
                //pieSeries.add("xxx", 28);
            }
        }
        else
        {
            barSeries.add("应用", 0);
        }
		barDataset.addSeries(barSeries.toXYSeries());
		return barDataset;
	}

	/**
	 * 构造渲染器
	 * @return
	 */
	public XYMultipleSeriesRenderer getRenderer() {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		renderer.setChartTitle("应用使用时间");
		renderer.setXTitle("应用");
		renderer.setYTitle("时间");
		renderer.setAxesColor(Color.WHITE);
		renderer.setLabelsColor(Color.WHITE);
		// 设置X轴的最小数字和最大数字，由于我们的数据是从1开始，所以设置为0.5就可以在1之前让出一部分
		// 有兴趣的童鞋可以删除下面两行代码看一下效果
		renderer.setXAxisMin(0.5);
		renderer.setXAxisMax(12.5);
		// 设置Y轴的最小数字和最大数字
		renderer.setYAxisMin(0);
		long total = capp.getTotalTimer();
		renderer.setYAxisMax(total);
		// 设置渲染器显示缩放按钮
		renderer.setZoomButtonsVisible(true);
		// 设置渲染器允许放大缩小
		renderer.setZoomEnabled(true);
		// 消除锯齿
		renderer.setAntialiasing(true);
		// 设置背景颜色
		renderer.setApplyBackgroundColor(true);
		renderer.setBackgroundColor(Color.GRAY);
		// 设置每条柱子的颜色
		SimpleSeriesRenderer sr = new SimpleSeriesRenderer();
		sr.setColor(Color.YELLOW);
		renderer.addSeriesRenderer(sr);
		// 设置每个柱子上是否显示数值
		//renderer.getSeriesRendererAt(0).setDisplayChartValues(true);
		int i = 1;
        Map<String,String> infomap = capp.GetMap();
        if(infomap!= null && infomap.size() > 0)
        {
        
            Iterator<String> it=infomap.keySet().iterator(); 
            while(it.hasNext()){ 
                String key; 
                String value; 
                key=it.next().toString(); 
                value=infomap.get(key); 
                long apptime = Long.parseLong(value);
                renderer.addXTextLabel(i, key);
                i++;
                //pieSeries.add("xxx", 28);
            }		
    		// X轴的近似坐标数
    		renderer.setXLabels(infomap.size());
        }
        else
        {
            renderer.addXTextLabel(1, "应用");
            renderer.setXLabels(1);
            renderer.setYAxisMax(10);
        }
		// Y轴的近似坐标数
		renderer.setYLabels(5);
		// 刻度线与X轴坐标文字左侧对齐
		renderer.setXLabelsAlign(Align.LEFT);
		// Y轴与Y轴坐标文字左对齐
		renderer.setYLabelsAlign(Align.LEFT);
		// 允许左右拖动,但不允许上下拖动.
		renderer.setPanEnabled(true, false);
		// 柱子间宽度
		renderer.setBarSpacing(0.1f);
		return renderer;
	}

}
