package com.hmx.chart;

import java.util.Iterator;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import com.hmx.phonetimeanalysis.HmxApplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

public class PieChart implements AChartAbstract {
    HmxApplication capp;
    public Intent getIntent(Context context,HmxApplication app) {
        capp = app;
		return ChartFactory.getPieChartIntent(context, getDataSet(), getPieRenderer(), "phone use times");
	}

	/**
	 * 构造饼图数据
	 */
	private CategorySeries getDataSet() {
		// 构造数据
		CategorySeries pieSeries = new CategorySeries("phone use times");
		Map<String,String> infomap = capp.GetMap();
		
		if(infomap!= null && infomap.size() > 0)
		{
    		long total = capp.getTotalTimer();
            Iterator<String> it=infomap.keySet().iterator(); 
            while(it.hasNext()){ 
                String key; 
                String value; 
                key=it.next().toString(); 
                value=infomap.get(key); 
                long apptime = Long.parseLong(value);
                pieSeries.add(key, (int)(apptime*100/total));
            }
		}
		return pieSeries;
	}

	/**
	 * 获取一个饼图渲染器
	 */
	private DefaultRenderer getPieRenderer() {
		// 构造一个渲染器
		DefaultRenderer renderer = new DefaultRenderer();
		// 设置渲染器显示缩放按钮
		renderer.setZoomButtonsVisible(true);
		// 设置渲染器允许放大缩小
		renderer.setZoomEnabled(true);
		// 设置渲染器标题文字大小
		renderer.setChartTitleTextSize(20);
		// 给渲染器增加3种颜色
	     Map<String,String> infomap = capp.GetMap();
	        
	     if(infomap!= null && infomap.size() > 0)
	     {
	         int size = infomap.size();
             for(int i = 0; i< size; i++)
             {
                 SimpleSeriesRenderer xRenderer = new SimpleSeriesRenderer();
                 xRenderer.setColor(Color.rgb(255, 255*i/size, 255*i/size));
                 renderer.addSeriesRenderer(xRenderer);
             }
	     }
		// 设置饼图文字字体大小和饼图标签字体大小
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		// 消除锯齿
		renderer.setAntialiasing(true);
		// 设置背景颜色
		renderer.setApplyBackgroundColor(true);
		renderer.setBackgroundColor(Color.BLACK);
		// 设置线条颜色
		renderer.setAxesColor(Color.WHITE);
		return renderer;
	}
}
